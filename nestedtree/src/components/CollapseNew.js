import React, { useState } from 'react'
import "./collapsCss.css"


export const CollapseNew = ({ collapsed, children, name }) => {

    const [isCollapsed, setIsCollapsed] = useState(collapsed);
    return (
        <div
               
        className="collapse-button accordion__title"
       
      >
      <p  onClick={() => setIsCollapsed(!isCollapsed)} > {isCollapsed ? '⇓ Show' : '⇑ Hide'}  {name} </p>
      
      <div
        className={`collapse-content ${isCollapsed ? 'collapsed' : 'expanded'}`}
        aria-expanded={isCollapsed}
      >
        {children}
      </div>



        </div>
    )
}
