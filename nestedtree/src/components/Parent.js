import React, { useState } from 'react'
import { items} from './api'
import { CollapseNew } from './CollapseNew'

export const Parent = () => {
    const [data ,setData] = useState(items)
    
    return (
       
         data.map((currData) =>{
             return (
             <div>
                 <ul>
                  <li className="accordion">
                     <CollapseNew name={currData.name} collapsed= {true} >
                        <ul className= "collapse-content-normal">
                         <li  >{currData.City}</li>
                        <li className="accordion" >
                        <CollapseNew name ={"Departments"} collapsed={true}>

                             <ul>
                                 {currData.departments.map((currDep) => {
                                 return   (
                                    
                                    
                                    <li><CollapseNew name ={currDep.name} collapsed={true}>
                                    <ul className= "collapse-content-normal">
                                    {currDep.others.map((currOther) => {
                                       return (<>
                                       
                                    
                                         {currOther.Students && <li>{currOther.Students}</li>}
                                        
                                         { currOther.Teachers && <li> {currOther.Teachers}</li>}
                                                </>)
                                    })}
                                    </ul>

                                        </CollapseNew>
                                    
                                    </li>
                                    
                                    
                                    
                                 )
                            })}
                           
                            </ul>
                        </CollapseNew>
                        </li>
                        </ul>
                      </CollapseNew>
                 </li>
                 </ul>
            
                </div>
             )// end of return
           
         })
           
            
                
      
        
    )//end of return
}
