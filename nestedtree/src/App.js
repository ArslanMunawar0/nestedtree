import { CollapseNew } from './components/CollapseNew';
import { Parent } from './components/Parent';
import "./components/collapsCss.css"
import './App.css';

function App() {
  return (
    <div className="App square">
      <div className="content">
      <Parent/>
      </div>
    </div>
  );
}

export default App;
